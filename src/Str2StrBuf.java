
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import antlr.collections.AST;

/*
 * Created on 02-nov-2004
 *
 */

/**
 * @author UF371231
 * 
 */
public class Str2StrBuf {

	public static void main(String[] args) {
		if(args.length>0)
			doFile(new File(args[0]));
		else
			System.out.println("Str2StrBuf what?");
	}

	public static void doFile(File f) {
		String cName=null;
		if (f.isDirectory()) {
			String files[] = f.list();
			for (int i = 0; i < files.length; i++)
				doFile(new File(f, files[i]));
		}
		// otherwise, if this is a java file, parse it!
		else if( (cName=isJavaFile(f.getName()))!=null )
		{
			try {
				AST ast=parseFile(f.getName());
				JavaEmitter.emit(mutate(ast,cName),System.out);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public static String isJavaFile(String fName){
		File f=new File(fName);
		if(   (f.getName().length() > 5)	
	    	&& f.getName().substring(f.getName().length() - 5).equals(".java")){
			return fName.substring(0,fName.length()-5).replace(File.pathSeparatorChar,'.');
		}
		return null;
	}
	
	// Here's where we do the real work...
	public static AST parseFile(String f) throws Exception {

		Reader r = new BufferedReader(new FileReader(f));

		JavaRecognizer parser = null;
		try {
			// Create a scanner that reads from the input stream passed to us
			JavaLexer lexer = new JavaLexer(r);
			lexer.setFilename(f);

			// Create a parser that reads from the scanner
			parser = new JavaRecognizer(lexer);
			parser.setFilename(f);

			// start parsing at the compilationUnit rule
			parser.compilationUnit();

		} catch (Exception e) {
			System.err.println("parser exception: " + e);
			e.printStackTrace(); // so we can get stack trace
		}

		return parser.getAST();
	}
	
	public static AST mutate(AST ori, String cName){
		Vector adiciones=new Vector();
		getAdditiveExpr(adiciones,ori);
		for(Iterator i=adiciones.iterator();i.hasNext();){
			AST semiRoot=(AST)i.next();
			if(isAnyStringOnExpr(semiRoot)){
				replaceAST(ori,semiRoot);
			}
		}
		return ori;
	}

	public static void getAdditiveExpr(Vector v, AST root){
		if(root.getType()==JavaTokenTypes.PLUS)
			v.add(root);
		for(AST child=root.getFirstChild();child!=null;child=child.getNextSibling()){
			getAdditiveExpr(v,child);
		}
	}
	
	public static boolean isAnyStringOnExpr(AST r){
		for(AST c=r.getFirstChild();c!=null;c=c.getNextSibling()){
			if(c.getType()==JavaTokenTypes.STRING_LITERAL)
				return true;
			//if(c.getType()==JavaTokenTypes.){
				
			//}
		}
		return false;
	}
	
	public static AST replaceAST(AST ori, AST semi){
		return ori;
	}
}