/* Copyright (c) 2003 Andy Tripp */

/**
  * A class that handles indenting nicely.
  * Can be used to output source code nicely, for example.
  * You wouldn't need a whole class for this, except for one problem:
  * After each newline, we need to indent to the appropriate level.
  * But we may then realize that we need to decrease the indent level,
  * but we've now already indented too far.
  * It's too late to "take back" one character from the input stream.
  */

import java.io.*;

public class IndentingPrintStream extends PrintStream {
	private String indentString = "\t";
	private int indent = 0;
	private boolean indentPending = false;

	public IndentingPrintStream(OutputStream out) {
		super(out);
	}

	public void setIndentString(String indentString) {
		this.indentString = indentString;
	}

	public void println() {
		super.println();
		indentPending = true;
	}

	public void increaseIndent() {
		indent++;
	}

	public void decreaseIndent() {
		indent--;
	}

	public void print(Object o) {
		print(o.toString());
	}

	public void print(String s) {
		if (indentPending) {
			for (int i=0; i<indent; i++) {
				super.print(indentString);
			}
			indentPending = false;
		}
		// handle newlines embedded in the string
		int index = s.indexOf("\n");
		if (index != -1) {
			super.print(s.substring(0, index));
			println();
			print(s.substring(index+1));
		}
		else {
			super.print(s);
		}
	}
}

	
